export interface ICompany {
    name: string;
    symbol: string;
    price: number;
    absolute_change?: number;
    percentage_of_change?: number;
    current_value?: number;
}