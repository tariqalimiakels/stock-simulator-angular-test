import { Injectable } from '@angular/core';
import { ApiService } from '../core/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class StockService {

  constructor(private api: ApiService) { }

  getCompanies() {
    return this.api.getRequest('stocks.php');
  }
}
