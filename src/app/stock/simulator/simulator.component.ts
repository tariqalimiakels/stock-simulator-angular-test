import { Component, OnInit } from '@angular/core';
import { StockService } from '../stock.service';
import { ICompany } from 'src/app/interfaces';

@Component({
  selector: 'app-simulator',
  templateUrl: './simulator.component.html',
  styleUrls: ['./simulator.component.scss']
})
export class SimulatorComponent implements OnInit {

  displayedColumns: string[] = ['name', 'symbol', 'inital_value', 'current_value', 'change_in_value'];
  data: ICompany[] = [];
  isLoadingResults: boolean = true;
  day: number = 1;
  simulatedDate: number = Date.now();
  
  constructor(private stockService: StockService) { }

  /**
   * Init Component
   * 
   * Return void
   */
  ngOnInit(): void {
    this.loadCompanies();
  }

  /**
   * Calculate Next Day projections
   * 
   * Return void
   */
  onNextDay(): void {
    this.projection();
    this.day++;
    const dateClicked = this.simulatedDate;
    const nextDay = new Date(dateClicked);
    nextDay.setDate(nextDay.getDate() + 1);
    this.simulatedDate = Number(nextDay);
  }

  /**
   * Load all companies
   * 
   * Return void
   */
  private loadCompanies(): void {

    this.stockService.getCompanies()
      .subscribe((res: ICompany[]) => {

        this.data = res;
        this.projection(true);
        this.isLoadingResults = false;
    }, err => {
        this.isLoadingResults = false;
    });
  }

  /**
   * Calculate projections
   * Params isFirstDay
   * 
   * Return void
   */
  private projection(isFirstDay: boolean = false): void {
    const data = this.data;
    
    const updatedData = data.map(element => {
      const currentValue = (isFirstDay) ? Number(element.price) : Number(element.current_value);
      const percentageOfChange = this.getRandomInt(10);
      const absoluteChange = Number(((currentValue / 100) * percentageOfChange).toFixed(2));
      const currentValueUpdated = Number((currentValue + absoluteChange).toFixed(2));

      return {
        ...element,
        absolute_change: absoluteChange,
        percentage_of_change: percentageOfChange,
        current_value: currentValueUpdated
      }
    });

    this.data = updatedData;
  }

  /**
   * Get Random No
   * Params max
   * 
   * Return number
   */
  private getRandomInt(max: number): number {
    return Math.floor(Math.random() * Math.floor(max));
  }

}
