import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimulatorComponent } from './simulator/simulator.component';
import { StockRoutingModule } from './stock-routing.module';
import { SharedModule } from '../shared/shared.module';
import { StockComponent } from './stock.component';

@NgModule({
  declarations: [
    SimulatorComponent,
    StockComponent,
  ],
  imports: [
    StockRoutingModule,
    SharedModule,
    CommonModule,
  ]
})
export class StockModule { }
