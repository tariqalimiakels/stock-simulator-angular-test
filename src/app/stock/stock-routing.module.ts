import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StockComponent } from "../stock/stock.component";
import { SimulatorComponent } from './simulator/simulator.component';

export const routes: Routes = [
  {
    path: '', component: StockComponent, children: [
      {
        path: 'simulation', component: SimulatorComponent
      },
      { path: '', pathMatch: 'full', redirectTo: 'simulation' },
    ]
  }
];
 

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StockRoutingModule { }