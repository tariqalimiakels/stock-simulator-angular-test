import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StockComponent } from "../stock/stock.component";
import { routes as stockRoutes } from "../stock/stock-routing.module";

const routes: Routes = [
  {
    path: "",
    component: StockComponent,
    children: stockRoutes
  },
  { path: "**", redirectTo: "/simulation" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
