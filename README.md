The idea is to make a simple stock simulator game in Angular 2+. You will be given 5 publicly traded companies with their initial stock values from a REST API and you must display information about them on a medium of your choice (table, cards, etc.., see Appendix). The companies will randomly change in value every day (days are counted from day 1 and the user can forward time with a click of a button)

## Run the DEMO

Make sure you have @angular/cli installed

```bash
  $ npm install -g @angular/cli
```

```bash
  $ cd stock-simulator-angular-test
  $ npm install
  $ npm start
```

the demo will be up at `localhost:4200`